(function () {
    function inheritense(parent, child) {
        var tempChild = child.prototype;

        if (Object.create) {
            child.prototype = Object.create(parent.prototype);
        } else {
            function F() {};
            F.prototype = parent.prototype;
            child.prototype = new F();
        }

        for (var key in tempChild) {
            if (tempChild.hasOwnProperty(key)) {
                child.prototype[key] = tempChild[key];
            }
        }
    }

    function Filter(elements) {
        this.DOMElements = elements;
        this.data = {};
    }


    Filter.prototype = {
        initListeners: function () {
            this.DOMElements.submitBtn.addEventListener("click", this.getFilters.bind(this))
        },
        getDateFilter: function () {
           this.data.date = this.DOMElements.dateFilter.value;
        },
        getPriceFilter: function () {
            this.data.price = this.DOMElements.priceFilter.value;
        },
        getFilters: function () {
            this.getPriceFilter();
            this.getDateFilter();
            this.showFilters();
        },
        showFilters: function() {
            this.DOMElements.resultArea.innerHTML = JSON.stringify(this.data);
        }
    };

    function ClockFilter(elements) {
        Filter.apply(this, arguments);
    }

    ClockFilter.prototype = {
        getGenderFilter: function() {
            this.data.gender = this.DOMElements.genderFilter.value;
        },
        getStyleFilter: function() {
            for (var i = 0; i < this.DOMElements.styleFilter.length; i++) {
                if (this.DOMElements.styleFilter[i].checked) {
                    this.data.style = this.DOMElements.styleFilter[i].value;
                }
            }
        },
        getFilters: function() {
            this.getGenderFilter();
            this.getStyleFilter();
            Filter.prototype.getFilters.call(this);
        }
    };

    inheritense(Filter, ClockFilter);

    function CarFilter(elements) {
        Filter.apply(this, arguments);
    }

    CarFilter.prototype = {
        getYearFilter: function() {
            this.data.year = this.DOMElements.yearFilter.value;
        },
        getManufacturerFilter: function() {
            this.data.manufacturer = [];
            var manufacturers = [];
            for (var i = 0; i < this.DOMElements.manufacturerFilter.length; i++) {
                if (this.DOMElements.manufacturerFilter[i].checked) {
                    manufacturers.push(this.DOMElements.manufacturerFilter[i].value);
                }
            }
            this.data.manufacturer = manufacturers;
        },
        getFilters: function() {
            this.getYearFilter();
            this.getManufacturerFilter();
            Filter.prototype.getFilters.call(this);
        }
    };

    inheritense(Filter, CarFilter);

    var form = document.querySelector('.filters-form');
    if(form.getAttribute('data-filter-type') === 'clock-filter') {
        var clockFilter = new ClockFilter({
            resultArea: document.querySelector('#filtersResult'),
            submitBtn: document.querySelector('#sortBtn'),
            priceFilter: document.querySelector('#priceFilter'),
            dateFilter: document.querySelector('#dateFilter'),
            genderFilter: document.querySelector('#genderFilter'),
            styleFilter: document.getElementsByName('styleFilter')
        });
        clockFilter.initListeners();
    } else if(form.getAttribute('data-filter-type') === 'cars-filter') {
        var carsFilter = new CarFilter({
            resultArea: document.querySelector('#filtersResult'),
            submitBtn: document.querySelector('#sortBtn'),
            priceFilter: document.querySelector('#priceFilter'),
            dateFilter: document.querySelector('#dateFilter'),
            yearFilter: document.querySelector('#yearFilter'),
            manufacturerFilter: document.getElementsByClassName('manufacturer-checkbox')
        });
        carsFilter.initListeners();
    }

})();
